var dictonary = { 
	"jd.jd"  : "JDownloader",
	"jd.delete"  : "L&ouml;schen",
	"jd.add"  : "Hinzuf&uuml;gen",
	"jd.edit"  : "Editieren",
	"jd.activate"  : "Aktivieren",
	"jd.deactivate"  : "Deaktivieren",
	"jd.renew"  : "Erneuern",
	"jd.options"  : "Optionen",
	
	
	"jd.speed"  : "Geschwindigkeit",
	"jd.speed.bps"  : "B/s",
	"jd.speed.kps"  : "KB/s",
	"jd.speed.mps"  : "MB/s",
	"jd.speed.gps"  : "GB/s",
	
	"nav.dashboard"  : "Dashboard",
	"nav.downloads"  : "Downloads",
	"nav.grabber"  : "Linkgrabber",
	"nav.passwords"  : "Passwortliste",
	"nav.accounts"  : "Accounts",
	"nav.settings"  : "Einstellungen",
	
	"dashboard.reports.successfull" : "Abgeschlossen",
	"dashboard.reports.failed" : "Fehlermeldungen",
	"dashboard.reports.done" : "Abgeschlossen in",
	"dashboard.reports.accounts" : "Aktive Accounts",
	
	
	"dashboard.downloads.title" : "Download &Uuml;bersicht",
	"dashboard.downloads.all" : "Downloads in der Liste",
	"dashboard.downloads.extracted" : "Entpackt",
	"dashboard.downloads.fail_extracted" : "Fehlerhaft Entpackt",
	"dashboard.downloads.done" : "Abgeschlossen",
	"dashboard.downloads.downloading" : "Am Herunterladen",
	"dashboard.downloads.fail" : "Fehlerhaft",
	
	
	"accounts.reports.title" : "Account",
	"accounts.reports.buy" : "Kaufen",
	
	"accounts.account.title" : "Accounts",
	"accounts.account.hoster" : "Anbieter",
	"accounts.account.user" : "Benutzername",
	"accounts.account.expire" : "Ablaufdatum",
	"accounts.account.status" : "Status",
	"accounts.account.traffic" : "Verbleibender Traffic",
	"accounts.account.active" : "Aktiv?",
	
	
	
	"downloads.reports.start" : "Starten",
	"downloads.reports.pause" : "Pausieren",
	"downloads.reports.stop" : "Stoppen",
	"downloads.reports.update" : "Update",
	"downloads.reports.shutdown" : "Beenden",
	
	"downloads.download.title" : "Downloads",
	"downloads.download.package" : "Paketname",
	"downloads.download.size" : "Gr&ouml;&szlig;e",
	"downloads.download.hoster" : "Anbieter",
	"downloads.download.progress" : "Fortschritt",
	
	
	"captcha.title" : "Captcha",
	"captcha.closed" : "Das Captcha wurde abgebrochen.",
	"captcha.send" : "Lösen",
}
$.i18n.setDictionary(dictonary);